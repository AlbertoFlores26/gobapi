<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use PDF;
use App\Cliente;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function imprimir(){
    	$cliente = Cliente::all();
    	$pdf = \PDF::loadView('pdf.pdfcita',  compact('cliente'));
    	return $pdf->download('ejemplo.pdf');
    }
}
