<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
  	 <img src="./images/seq2.png" class="imagen1"  alt="logo" width="230px" height="100px">
  	 <h5 style="text-align: center;">Secretaria de educación</h5>	
	<h5 style="text-align: center;">Servicios educativos de Quintana Roo</h5>	
  	@foreach($cliente as $dato)

	<h6>ID SOLICITUD: {{$dato->id}}</h6>
	<h6>Fecha de solicitud: {{$dato->created_at}}</h6> 
	<h6>Nombre: {{$dato->ews_nombre}}</h6>	
	<h6>Apellido Paterno: {{$dato->ews_apellido_paterno}}</h6>
	<h6>Apellido Materno: {{$dato->ews_apellido_materno}}</h6>
	<h6>Trámite: {{$dato->ews_tramite}}</h6>
	<h6>Correo: {{$dato->ews_email}}</h6>

  	@endforeach

  <br>

<p>Para la validación posterior del trámite, favor de pasar a ventanilla a dejar los requisitos en físico solicitados.</p>

  <br>

 <h6>Dirección: </h6>

 <br>

 <p>De Las Calandrias 20, Tumben Cuxtal, 32, 91, 77516 Cancún, Q.R.</p>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>