<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function() {
	return view('welcome');
});

Route::resource('/registro', 'ClienteController');

Route::resource('/escuelasup', 'SuperiorController');

Route::resource('/escuelamedia', 'MediaController');

Route::resource('/escuelanormal', 'NormalController');

Route::resource('/areajuridica', 'JuridicaController');

Route::resource('/areacademica', 'AcademicaController');

Route::name('imprimir')->get('/imprimir', 'Controller@imprimir');


