<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuperiorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('superiors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ews_solici_formato');
            $table->string('ews_tema_nombre');
            $table->string('ews_acta_constitutiva');
            $table->string('ews_carta_obligato');
            $table->string('ews_anexo1');
            $table->string('ews_anexo3');
            $table->string('ews_mapa_curricular');
            $table->string('ews_lista_acervos');
            $table->string('ews_regla_plantel');
            $table->integer('ews_id_creador')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('superiors');
    }
}
