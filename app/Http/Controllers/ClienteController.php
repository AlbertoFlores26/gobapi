<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Cliente;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $json = array(

            "wsp_mensaje" => "No encontrado",
        );

        return json_encode($json, true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datos = array("ews_nombre" => $request->input("ews_nombre"), 
                       "ews_apellido_paterno" => $request->input("ews_apellido_paterno"),
                       "ews_apellido_materno" => $request->input("ews_apellido_materno"),
                       "ews_curp" => $request->input("ews_curp"),
                       "ews_tramite" => $request->input("ews_tramite"),
                       "ews_persona" => $request->input("ews_persona"),
                       "ews_email" => $request->input("ews_email"));


        /*==========================================================
        =            Validar si los campos estan vacios            =
        ==========================================================*/

        if(!empty($datos))
        {

            //Validar datos
             
             $validator = Validator::make($datos, [
                'ews_nombre' => 'required|string|max:255',
                'ews_apellido_paterno' => 'required|string|max:255',
                'ews_apellido_materno' => 'required|string|max:255',
                'ews_curp' => 'required|string|max:255|unique:clientes',
                'ews_tramite' => 'required|string|max:255',
                'ews_persona' => 'required|string|max:255',
                'ews_email' => 'required|string|email|max:255|unique:clientes'
            ]);

             if ($validator->fails()) {
               $json = array(

                "wsp_status" =>404,
                "wsp_mensaje" => "Registros no validos: posible formulario vacio, datos incorrectos o ya existentes"
            );

            return json_encode($json, true);
            
            }else
            {


    /*====================================================
    =            Guardado en la base de datos            =
    ====================================================*/



    /*=====   Guardado en la base de datos  ======*/
            
            $ews_id_cliente = Hash::make($datos["ews_nombre"].$datos["ews_apellido_paterno"].$datos["ews_email"]);
            $ews_llave_secreta = Hash::make($datos["ews_email"].$datos["ews_apellido_paterno"].$datos["ews_nombre"], ['rounds' => 12]);

            $cliente = new Cliente();
            $cliente->ews_nombre = $datos["ews_nombre"];
            $cliente->ews_apellido_paterno = $datos["ews_apellido_paterno"];
            $cliente->ews_apellido_materno = $datos["ews_apellido_materno"];
            $cliente->ews_curp = $datos["ews_curp"];
            $cliente->ews_tramite = $datos["ews_tramite"];
            $cliente->ews_persona = $datos["ews_persona"];
            $cliente->ews_email = $datos["ews_email"];
            $cliente->ews_id_cliente = str_replace('$', '-', $ews_id_cliente); 
            $cliente->ews_llave_secreta = str_replace('$', '-', $ews_llave_secreta);
            $cliente->save();

             $json = array(

                "wsp_status" =>200,
                "wsp_mensaje" => "Registro exitoso, tome sus credenciales y guardelas",
                "wsp_credenciales" => array("wsp_id_cliente" => str_replace('$', '-', $ews_id_cliente), "wsp_llave_secreta" => str_replace('$', '-', $ews_llave_secreta))
            );

             return json_encode($json, true);
         

          /*  $id_cliente = Hash::make($datos["primer_nombre"].$datos["primer_apellido"].$datos["email"]);
            $llave_secreta = Hash::make($datos["email"].$datos["primer_apellido"].$datos["primer_nombre"], [
                            'rounds' => 12
                        ]);

            echo '<pre>'; print_r($id_cliente); echo '</pre>';
            echo '<pre>'; print_r($llave_secreta); echo '</pre>'; */

            //echo '<pre>'; print_r($datos); echo '</pre>';

            }
        }else
        
        {
            $json = array(
                "wsp_status" =>404,
                "wsp_mensaje" => "Registros incompletos"
            );

            return json_encode($json, true);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
