<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ews_nombre');
            $table->string('ews_apellido_paterno');
            $table->string('ews_apellido_materno');
            $table->string('ews_curp');
            $table->string('ews_tramite');
            $table->string('ews_persona');
            $table->string('ews_email');
            $table->text('ews_id_cliente');
            $table->text('ews_llave_secreta');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
